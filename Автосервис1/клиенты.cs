﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Автосервис1
{
    public partial class клиенты : Form
    {
        public клиенты()
        {
            InitializeComponent();
        }

        private void клиенты_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "автосервисDataSet.Gender". При необходимости она может быть перемещена или удалена.
            this.genderTableAdapter.Fill(this.автосервисDataSet.Gender);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "автосервисDataSet.Client". При необходимости она может быть перемещена или удалена.
            this.clientTableAdapter.Fill(this.автосервисDataSet.Client);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Меню меню = new Меню();
            меню.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
