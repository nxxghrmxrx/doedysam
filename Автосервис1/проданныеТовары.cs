﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Автосервис1
{
    public partial class проданныеТовары : Form
    {
        public проданныеТовары()
        {
            InitializeComponent();
        }

        private void проданныеТовары_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "автосервисDataSet.Client". При необходимости она может быть перемещена или удалена.
            this.clientTableAdapter.Fill(this.автосервисDataSet.Client);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "автосервисDataSet.ClientService". При необходимости она может быть перемещена или удалена.
            this.clientServiceTableAdapter.Fill(this.автосервисDataSet.ClientService);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "автосервисDataSet.Product". При необходимости она может быть перемещена или удалена.
            this.productTableAdapter.Fill(this.автосервисDataSet.Product);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "автосервисDataSet.ProductSale". При необходимости она может быть перемещена или удалена.
            this.productSaleTableAdapter.Fill(this.автосервисDataSet.ProductSale);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Меню меню = new Меню();
            меню.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Validate();
            productBindingSource.EndEdit();
            productTableAdapter.Update(автосервисDataSet);
            productTableAdapter.Fill(автосервисDataSet.Product);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
