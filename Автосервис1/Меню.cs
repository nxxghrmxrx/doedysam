﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Автосервис1
{
    public partial class Меню : Form
    {
        public Меню()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Товары товары = new Товары();
            товары.Show();
            Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            проданныеТовары проданныеТовары = new проданныеТовары();
            проданныеТовары.Show();
            Hide();
        }

        private void Меню_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Производитель производитель = new Производитель();
            производитель.Show();
            Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            клиенты клиенты = new клиенты();
            клиенты.Show();
            Hide();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Авторизация авторизация = new Авторизация();
            авторизация.Show();
            Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
